(* Once you are done writing the code, remove this directive,
   whose purpose is to disable several warnings. *)
[@@@warning "-27-32-33-34-37-39"]

open Functor

module Make (F : Functor) = struct
  module Base = struct
    type 'a t =
      | Return of 'a
      | Op of 'a t F.t

    let return a = failwith "NYI"

    let rec bind m f = failwith "NYI"
  end

  module M = Monad.Expand (Base)
  include M
  open Base

  let op xs = Op (F.map return xs)

  type ('a, 'b) algebra =
    { return : 'a -> 'b
    ; op : 'b F.t -> 'b
    }

  let rec run alg m = failwith "NYI"
end
